#introduction of R_data
head() 
tail()
view()
dim()
str()
summary()

# using if function

if(condition1) {
statement / print ("non-nagative number")
}


if(time$x1 >= 2) {
time$x2 <- time$x2 +12
}
      ID    x1    x2
   <dbl> <dbl> <dbl>
 1     1     1    10
 2     2     2    25
 3     3     2     8
 4     4     2     5
 5     5     1     8

after run coding

      ID    x1    x2
   <dbl> <dbl> <dbl>
 1     1     1    10
 2     2     2    37
 3     3     2    20
 4     4     2    17
 5     5     1     8

<details><summary>Click to expand</summary>

</details>
x <- -5
if(x > 0){
print("Non-negative number")
} else {
print("Negative number")
}

#count data and Export file 
count(data_name, column_name)
export(data_name, "name of file. csv/xlsx)

#count NA data
sum(is.na(data_name$column_name))          # by one column
data_name%>%                               #count all in one time
  summarise_all(funs(sum(is.na(.)))) or 

sapply(data_name, function(x) sum(is.na(x)))  # count all in one time

# remove NA data with specific column
delete_na <- filter(data_name, !is.na(data$column_name)) # with library(dplyr)


# joint_left
New_name_data<-original_data %>% 
  left_join(_new_data, by = c("name_column_orginal_data" = "name_column_new_data"))

# count the commuting time

#joint table (row)
combine data_name1 + data_name2

data_name <- data_name1 %>% 
  bind_rows(data_name2,id=NULL)


